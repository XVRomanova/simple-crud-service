package config

import (
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	DB DataBaseConfig `mapstructure:"database"`
}

type DataBaseConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	User     string `mapstructure:"user"`
	Password string `mapstructure:"password"`
	Name     string `mapstructure:"name"`
}

func New() *Config {
	config := Config{
		DB: DataBaseConfig{},
	}

	v := viper.New()
	v.SetConfigName("config")
	v.AddConfigPath(".")

	err := v.ReadInConfig()
	if err != nil {
		log.Fatal()
	}

	err = v.Unmarshal(&config)
	if err != nil {
		log.Fatal()
	}

	return &config
}
