package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/XVRomanova/simple-crud-service/domain/models"
)

type Controller struct {
	service Service
}

type Service interface {
	GetStructByID(id []string) ([]models.Content, error)
}

func New(service Service) *Controller {
	controller := Controller{
		service: service,
	}

	return &controller
}

func (controller *Controller) ReadOrders(writer http.ResponseWriter, request *http.Request) {
	bodyRequest := request.URL.Query()

	id := bodyRequest["id"]

	orders, err := controller.service.GetStructByID(id)
	if err != nil {
		log.Fatal(err)
	}

	bodyBytes, err := json.Marshal(orders)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprint(writer, string(bodyBytes))
}
