module gitlab.com/XVRomanova/simple-crud-service

go 1.16

require (
	github.com/lib/pq v1.10.2
	github.com/spf13/viper v1.8.1 // indirect
)
