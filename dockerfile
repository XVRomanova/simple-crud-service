FROM golang
COPY config.toml .
COPY simple-crud-service .
RUN chmod +x simple-crud-service
ENTRYPOINT "./simple-crud-service"