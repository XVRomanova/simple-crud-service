package services

import (
	"log"
	"time"

	"gitlab.com/XVRomanova/simple-crud-service/domain/models"
)

type Service struct {
	repo     Repository
	provider Provider
}

type Repository interface {
	PutNewStruct(data models.Data) error
	GetStructByID(id []string) ([]models.Content, error)
	DeleteDataFromTable() error
}

type Provider interface {
	GetStruct() (models.Data, error)
}

func New(repo Repository, provider Provider) *Service {
	service := Service{
		repo:     repo,
		provider: provider,
	}

	return &service
}

func (service Service) PutStructIntoDataBase() error {
	order, err := service.provider.GetStruct()
	if err != nil {
		return err
	}

	err = service.repo.PutNewStruct(order)
	if err != nil {
		return err
	}

	return nil
}

func (service Service) GetStructByID(id []string) ([]models.Content, error) {
	orders, err := service.repo.GetStructByID(id)
	if err != nil {
		return nil, err
	}

	return orders, nil
}

func (service Service) DeletePreviousRowVersion() error {
	err := service.repo.DeleteDataFromTable()
	if err != nil {
		return err
	}

	return nil
}

func (service Service) SynchronizeAPIAndDB() {
	t := time.NewTimer(3 * time.Minute)

	for {
		err := service.DeletePreviousRowVersion()
		if err != nil {
			log.Fatal(err)
		}

		err = service.PutStructIntoDataBase()
		if err != nil {
			log.Fatal(err)
		}

		<-t.C
	}
}
