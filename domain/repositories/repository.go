package repositories

import (
	"database/sql"
	"fmt"

	"gitlab.com/XVRomanova/simple-crud-service/config"

	"log"
	"strconv"

	"gitlab.com/XVRomanova/simple-crud-service/domain/models"
)

type Repository struct {
	db *sql.DB
}

func New(config *config.Config) *Repository {
	psqlconn := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.DB.Host,
		config.DB.Port,
		config.DB.User,
		config.DB.Password,
		config.DB.Name,
	)

	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Fatal(err)
	}

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	repo := Repository{
		db: db,
	}

	return &repo
}

func (repo *Repository) PutNewStruct(data models.Data) error {
	arrayOfContent := data.Content

	for _, currentContent := range arrayOfContent {
		_, err := repo.db.Exec(
			"INSERT INTO ORDERS(row_version, order_id, status, store_id, data_created) VALUES ($1, $2, $3, $4, $5)",
			data.RowVersion,
			currentContent.OrderID,
			currentContent.Status,
			currentContent.StoreID,
			currentContent.DateCreated,
		)

		if err != nil {
			return err
		}
	}

	return nil
}

func (repo *Repository) DeleteDataFromTable() error {
	sqlStatement := "TRUNCATE orders"

	_, err := repo.db.Exec(sqlStatement)
	if err != nil {
		return err
	}

	return nil
}

func (repo *Repository) GetStructByID(orderIDs []string) ([]models.Content, error) {
	sqlStatement := "SELECT * FROM orders"

	rows, err := repo.db.Query(sqlStatement)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	orders := make(map[int]models.Content)

	for rows.Next() {
		data := models.Data{}

		content := models.Content{}

		err = rows.Scan(&data.RowVersion, &content.OrderID, &content.Status, &content.StoreID, &content.DateCreated)
		if err != nil {
			return nil, err
		}

		orders[content.OrderID] = content
	}

	contents := make([]models.Content, 0)

	for _, orderID := range orderIDs {
		intOrderID, err := strconv.Atoi(orderID)
		if err != nil {
			return nil, err
		}

		if content, ok := orders[intOrderID]; ok {
			contents = append(contents, content)
		}
	}

	return contents, nil
}
