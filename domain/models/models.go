package models

type Data struct {
	RowVersion int       `json:"rv"`
	Content    []Content `json:"content"`
}

type Content struct {
	OrderID     int    `json:"order_id"`
	Status      string `json:"status"`
	StoreID     int    `json:"store_id"`
	DateCreated string `json:"date_created"`
}
