package providers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/XVRomanova/simple-crud-service/domain/models"
)

type Provider struct {
}

func New() *Provider {
	provider := Provider{}
	return &provider
}

func (provider *Provider) GetStruct() (models.Data, error) {
	var data models.Data

	resp, err := http.Get("http://localhost:8081/")
	if err != nil {
		return data, err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return data, err
	}

	err = json.Unmarshal(bodyBytes, &data)
	if err != nil {
		return data, err
	}

	return data, nil
}
