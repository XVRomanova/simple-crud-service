package main

import (
	"log"
	"net/http"

	"gitlab.com/XVRomanova/simple-crud-service/config"

	_ "github.com/lib/pq"
	"gitlab.com/XVRomanova/simple-crud-service/application/services"
	"gitlab.com/XVRomanova/simple-crud-service/controllers"
	"gitlab.com/XVRomanova/simple-crud-service/domain/providers"
	"gitlab.com/XVRomanova/simple-crud-service/domain/repositories"
)

func main() {
	cfg := config.New()
	repo := repositories.New(cfg)
	provider := providers.New()
	service := services.New(repo, provider)
	controller := controllers.New(service)

	go service.SynchronizeAPIAndDB()

	http.HandleFunc("/orders", controller.ReadOrders)

	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}
